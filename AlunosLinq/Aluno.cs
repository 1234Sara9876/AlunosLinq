﻿using System;
using Modelos;

public class Aluno
{
    #region atributos e propriedades

    public int IdAluno { get; set; }

    public string PrimeiroNome { get; set; }

    public string Apelido { get; set; }

    public string NomeCompleto
    {
        get { return string.Format("{0} {1}", PrimeiroNome, Apelido); }
    }
    #endregion

    #region Métodos Genéricos

    public override string ToString()
    {
        return string.Format("Id Aluno: {0}  Nome: {1}", IdAluno,     NomeCompleto);
	}
}
