﻿
using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //using ClassLibrary1 é o mmo q criar uma refª neste projeto

            List<Aluno> Alunos = ListaDeAlunos.loadLista();
            //declara-se o objeto mas n se instancia pq a classe é estática

            //Ordenar por apelido
            Alunos = Alunos.OrderBy(x => x.Apelido).ToList();
            //o Linq faz um override da lista de origem; a lista gerada é do tipo IEnumerable por isso precisa do ToList para transformá-la numa nova lista comum i.é genérica
            //tb há OrderByAscending e OrderByDescending e para condição de desempate, o ThenBy, ou ThenByAscending, ou ThenByDescending 

            //Fazer uma pesquisa ao aluno
            Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList();

            foreach (var a in Alunos)
            {
                Console.WriteLine($"{a.PrimeiroNome}{a.Apelido}({a.DataNascimento.ToShortDateString()}) Disciplinas feitas: {a.DisciplinasFeitas}");
                //os parêntesis à volta da data vão aparecer, tal como a expressão Disciplinas feitas, pq n fazem parte da sintaxe
            }
            int totalDFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            //soma apenas os elementos da última lista declarada;
            //tb há a double m = Alunos.Average
            totalDFeitas = Alunos.Where(x => x.DataNascimento.Month == 2).Sum(x => x.DisciplinasFeitas);
            //soma apenas os elementos q correspondem a uma condição
            
            Console.ReadKey();
        }
    }
}
