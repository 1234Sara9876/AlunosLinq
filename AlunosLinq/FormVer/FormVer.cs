﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormVer
{
    public partial class FormVer : Form
    {
        List<Aluno> alunoscopiados;
        public FormVer(List<Aluno> alunos)
        {
            InitializeComponent();
            alunoscopiados = alunos;
            InitCombo();
        }
        //Procurar alunos por nome e por nº?
        //Editar e cancelar edição através do enable e disable dos controlos?
        private void InitCombo()
        {

            cbTodosAlunos.DataSource = alunoscopiados;
            //sintaxe do display: a propriedade-atributo entre aspas
            cbTodosAlunos.DisplayMember = "NomeCompleto";
            libFiltro.DataSource = alunoscopiados.Where(x => x.DisciplinasFeitas > 10).OrderBy(x => x.PrimeiroNome).ThenBy(x => x.Apelido).ToList();
            libFiltro.DisplayMember = "NomeCompleto";
        }
        private void cbTodosAlunos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)cbTodosAlunos.SelectedItem;
            udFeitas.Value = alunoSelecionado.DisciplinasFeitas;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Aluno alunoSelecionado = (Aluno)cbTodosAlunos.SelectedItem;
            alunoSelecionado.DisciplinasFeitas = Convert.ToInt32(udFeitas.Value);
            UpdateData();
            //atualiza a lista e reordena
        }

        private void UpdateData()
        {
            libFiltro.DataSource = alunoscopiados.Where(x => x.DisciplinasFeitas > 10).OrderBy(x => x.PrimeiroNome).ThenBy(x => x.Apelido).ToList();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        //Validações do CRUD antes de gravar?
        //Actualiza-se os combos?
    }
}
