﻿namespace FormVer
{
    partial class FormVer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.libFiltro = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.udFeitas = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTodosAlunos = new System.Windows.Forms.ComboBox();
            this.lblTodosAlunos = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.udFeitas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // libFiltro
            // 
            this.libFiltro.FormattingEnabled = true;
            this.libFiltro.ItemHeight = 17;
            this.libFiltro.Location = new System.Drawing.Point(343, 42);
            this.libFiltro.Name = "libFiltro";
            this.libFiltro.Size = new System.Drawing.Size(220, 225);
            this.libFiltro.TabIndex = 27;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(393, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 26;
            this.label2.Text = "Alunos filtrados";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(33, 244);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 23);
            this.btnUpdate.TabIndex = 25;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // udFeitas
            // 
            this.udFeitas.Location = new System.Drawing.Point(197, 194);
            this.udFeitas.Name = "udFeitas";
            this.udFeitas.Size = new System.Drawing.Size(72, 23);
            this.udFeitas.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 23;
            this.label1.Text = "Disciplinas feitas";
            // 
            // cbTodosAlunos
            // 
            this.cbTodosAlunos.FormattingEnabled = true;
            this.cbTodosAlunos.Location = new System.Drawing.Point(33, 42);
            this.cbTodosAlunos.Name = "cbTodosAlunos";
            this.cbTodosAlunos.Size = new System.Drawing.Size(236, 25);
            this.cbTodosAlunos.TabIndex = 22;
            // 
            // lblTodosAlunos
            // 
            this.lblTodosAlunos.AutoSize = true;
            this.lblTodosAlunos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTodosAlunos.Location = new System.Drawing.Point(30, 22);
            this.lblTodosAlunos.Name = "lblTodosAlunos";
            this.lblTodosAlunos.Size = new System.Drawing.Size(128, 17);
            this.lblTodosAlunos.TabIndex = 21;
            this.lblTodosAlunos.Text = "Todos os alunos";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(33, 292);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(530, 133);
            this.dataGridView1.TabIndex = 28;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // FormVer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.libFiltro);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.udFeitas);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbTodosAlunos);
            this.Controls.Add(this.lblTodosAlunos);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormVer";
            this.Text = "Consultar Alunos";
            ((System.ComponentModel.ISupportInitialize)(this.udFeitas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox libFiltro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.NumericUpDown udFeitas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTodosAlunos;
        private System.Windows.Forms.Label lblTodosAlunos;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}

