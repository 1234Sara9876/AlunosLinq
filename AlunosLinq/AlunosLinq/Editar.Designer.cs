﻿namespace AlunosLinq
{
    partial class Editar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Editar));
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.tbApelido = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.udFeitas = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.tbDataNasc = new System.Windows.Forms.TextBox();
            this.tbPrimeiroNome = new System.Windows.Forms.TextBox();
            this.tbIdAluno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFechar3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.udFeitas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGravar
            // 
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.Location = new System.Drawing.Point(294, 252);
            this.btnGravar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(33, 36);
            this.btnGravar.TabIndex = 30;
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnForward
            // 
            this.btnForward.Image = ((System.Drawing.Image)(resources.GetObject("btnForward.Image")));
            this.btnForward.Location = new System.Drawing.Point(168, 242);
            this.btnForward.Margin = new System.Windows.Forms.Padding(2);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(26, 36);
            this.btnForward.TabIndex = 29;
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // btnBack
            // 
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(80, 242);
            this.btnBack.Margin = new System.Windows.Forms.Padding(2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(23, 36);
            this.btnBack.TabIndex = 28;
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // tbApelido
            // 
            this.tbApelido.Location = new System.Drawing.Point(110, 136);
            this.tbApelido.Name = "tbApelido";
            this.tbApelido.Size = new System.Drawing.Size(239, 20);
            this.tbApelido.TabIndex = 41;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 40;
            this.label5.Text = "Apelido";
            // 
            // udFeitas
            // 
            this.udFeitas.Location = new System.Drawing.Point(283, 24);
            this.udFeitas.Name = "udFeitas";
            this.udFeitas.ReadOnly = true;
            this.udFeitas.Size = new System.Drawing.Size(66, 20);
            this.udFeitas.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(192, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Disciplinas feitas";
            // 
            // tbDataNasc
            // 
            this.tbDataNasc.Location = new System.Drawing.Point(139, 199);
            this.tbDataNasc.Name = "tbDataNasc";
            this.tbDataNasc.Size = new System.Drawing.Size(210, 20);
            this.tbDataNasc.TabIndex = 37;
            // 
            // tbPrimeiroNome
            // 
            this.tbPrimeiroNome.Location = new System.Drawing.Point(110, 76);
            this.tbPrimeiroNome.Name = "tbPrimeiroNome";
            this.tbPrimeiroNome.Size = new System.Drawing.Size(239, 20);
            this.tbPrimeiroNome.TabIndex = 36;
            // 
            // tbIdAluno
            // 
            this.tbIdAluno.Location = new System.Drawing.Point(80, 24);
            this.tbIdAluno.Name = "tbIdAluno";
            this.tbIdAluno.ReadOnly = true;
            this.tbIdAluno.Size = new System.Drawing.Size(39, 20);
            this.tbIdAluno.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Data de Nascimento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Primeiro Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 32;
            this.label1.Text = "ID Aluno";
            // 
            // btnFechar3
            // 
            this.btnFechar3.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar3.Image")));
            this.btnFechar3.Location = new System.Drawing.Point(332, 254);
            this.btnFechar3.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar3.Name = "btnFechar3";
            this.btnFechar3.Size = new System.Drawing.Size(36, 33);
            this.btnFechar3.TabIndex = 42;
            this.btnFechar3.UseVisualStyleBackColor = true;
            this.btnFechar3.Click += new System.EventHandler(this.btnFechar3_Click);
            // 
            // Editar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 303);
            this.ControlBox = false;
            this.Controls.Add(this.btnFechar3);
            this.Controls.Add(this.tbApelido);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.udFeitas);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbDataNasc);
            this.Controls.Add(this.tbPrimeiroNome);
            this.Controls.Add(this.tbIdAluno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.btnBack);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Editar";
            this.Text = "Editar";
            ((System.ComponentModel.ISupportInitialize)(this.udFeitas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.TextBox tbApelido;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown udFeitas;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbDataNasc;
        private System.Windows.Forms.TextBox tbPrimeiroNome;
        private System.Windows.Forms.TextBox tbIdAluno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFechar3;
    }
}