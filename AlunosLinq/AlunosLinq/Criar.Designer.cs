﻿namespace AlunosLinq
{
    partial class Criar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Criar));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnFechar1 = new System.Windows.Forms.Button();
            this.tbDataNasc = new System.Windows.Forms.TextBox();
            this.tbPrimeiroNome = new System.Windows.Forms.TextBox();
            this.tbIdAluno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.udFeitas = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.tbApelido = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.udFeitas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(278, 266);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(40, 33);
            this.btnSave.TabIndex = 21;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnFechar1
            // 
            this.btnFechar1.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar1.Image")));
            this.btnFechar1.Location = new System.Drawing.Point(332, 266);
            this.btnFechar1.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar1.Name = "btnFechar1";
            this.btnFechar1.Size = new System.Drawing.Size(36, 33);
            this.btnFechar1.TabIndex = 20;
            this.btnFechar1.UseVisualStyleBackColor = true;
            this.btnFechar1.Click += new System.EventHandler(this.btnFechar1_Click);
            // 
            // tbDataNasc
            // 
            this.tbDataNasc.Location = new System.Drawing.Point(146, 216);
            this.tbDataNasc.Name = "tbDataNasc";
            this.tbDataNasc.Size = new System.Drawing.Size(104, 20);
            this.tbDataNasc.TabIndex = 27;
            // 
            // tbPrimeiroNome
            // 
            this.tbPrimeiroNome.Location = new System.Drawing.Point(117, 93);
            this.tbPrimeiroNome.Name = "tbPrimeiroNome";
            this.tbPrimeiroNome.Size = new System.Drawing.Size(239, 20);
            this.tbPrimeiroNome.TabIndex = 26;
            // 
            // tbIdAluno
            // 
            this.tbIdAluno.Location = new System.Drawing.Point(87, 41);
            this.tbIdAluno.Name = "tbIdAluno";
            this.tbIdAluno.ReadOnly = true;
            this.tbIdAluno.Size = new System.Drawing.Size(39, 20);
            this.tbIdAluno.TabIndex = 25;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 219);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Data de Nascimento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Primeiro Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "ID Aluno";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(199, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Disciplinas feitas";
            // 
            // udFeitas
            // 
            this.udFeitas.Location = new System.Drawing.Point(290, 41);
            this.udFeitas.Name = "udFeitas";
            this.udFeitas.ReadOnly = true;
            this.udFeitas.Size = new System.Drawing.Size(66, 20);
            this.udFeitas.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(33, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Apelido";
            // 
            // tbApelido
            // 
            this.tbApelido.Location = new System.Drawing.Point(117, 153);
            this.tbApelido.Name = "tbApelido";
            this.tbApelido.Size = new System.Drawing.Size(239, 20);
            this.tbApelido.TabIndex = 31;
            // 
            // Criar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 317);
            this.ControlBox = false;
            this.Controls.Add(this.tbApelido);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.udFeitas);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbDataNasc);
            this.Controls.Add(this.tbPrimeiroNome);
            this.Controls.Add(this.tbIdAluno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnFechar1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Criar";
            this.Text = "Criar";
            ((System.ComponentModel.ISupportInitialize)(this.udFeitas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnFechar1;
        private System.Windows.Forms.TextBox tbDataNasc;
        private System.Windows.Forms.TextBox tbPrimeiroNome;
        private System.Windows.Forms.TextBox tbIdAluno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown udFeitas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbApelido;
    }
}