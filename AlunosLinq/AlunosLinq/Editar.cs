﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace AlunosLinq
{
    public partial class Editar : Form
    {
        List<Aluno> alunoscopiados;
        int index=0;

        public Editar(List<Aluno> alunos)
        {
            InitializeComponent();
            alunoscopiados = alunos;
            if (alunoscopiados.Count > 0)
                EditAlunos();
            else
                MessageBox.Show("Não há alunos inscritos");
        }

        private void EditAlunos()
        {
            tbIdAluno.Text = alunoscopiados[index].IdAluno.ToString();
            tbPrimeiroNome.Text = alunoscopiados[index].PrimeiroNome;
            tbApelido.Text = alunoscopiados[index].Apelido;
            tbDataNasc.Text = alunoscopiados[index].DataNascimento.ToString();
            udFeitas.Value = alunoscopiados[index].DisciplinasFeitas;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (index > 0)
            {
                index--;
                EditAlunos();
            }
               
            else
                MessageBox.Show("A lista não tem mais itens");
        }
        
        private void btnForward_Click(object sender, EventArgs e)
        {
            if (index < alunoscopiados.Count - 1)
            {
                index++;
                EditAlunos();
            }
            else
                MessageBox.Show("A lista não tem mais itens");
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < alunoscopiados.Count; i++)
            //    if (alunoscopiados[i].IdAluno==Convert.ToInt16(tbIdAluno.Text))

            alunoscopiados[index].IdAluno = index + 1;
            alunoscopiados[index].PrimeiroNome = tbPrimeiroNome.Text;
            alunoscopiados[index].Apelido = tbApelido.Text;
            alunoscopiados[index].DataNascimento = Convert.ToDateTime(tbDataNasc.Text);
            alunoscopiados[index].DisciplinasFeitas = Convert.ToInt16(udFeitas.Value);
            MessageBox.Show("Dados de aluno actualizados com sucesso");
            Close();
        }

        private void btnFechar3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
