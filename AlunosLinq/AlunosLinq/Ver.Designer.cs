﻿namespace AlunosLinq
{
    partial class Ver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ver));
            this.dgvAluno = new System.Windows.Forms.DataGridView();
            this.lboxFiltro = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTodosAlunos = new System.Windows.Forms.ComboBox();
            this.lblTodosAlunos = new System.Windows.Forms.Label();
            this.btnFechar2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAluno)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAluno
            // 
            this.dgvAluno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAluno.Location = new System.Drawing.Point(21, 270);
            this.dgvAluno.Margin = new System.Windows.Forms.Padding(2);
            this.dgvAluno.Name = "dgvAluno";
            this.dgvAluno.RowTemplate.Height = 24;
            this.dgvAluno.Size = new System.Drawing.Size(560, 46);
            this.dgvAluno.TabIndex = 36;
            // 
            // lboxFiltro
            // 
            this.lboxFiltro.FormattingEnabled = true;
            this.lboxFiltro.Location = new System.Drawing.Point(307, 24);
            this.lboxFiltro.Margin = new System.Windows.Forms.Padding(2);
            this.lboxFiltro.Name = "lboxFiltro";
            this.lboxFiltro.Size = new System.Drawing.Size(273, 212);
            this.lboxFiltro.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(304, 9);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Alunos filtrados";
            // 
            // cbTodosAlunos
            // 
            this.cbTodosAlunos.FormattingEnabled = true;
            this.cbTodosAlunos.Location = new System.Drawing.Point(20, 24);
            this.cbTodosAlunos.Margin = new System.Windows.Forms.Padding(2);
            this.cbTodosAlunos.Name = "cbTodosAlunos";
            this.cbTodosAlunos.Size = new System.Drawing.Size(200, 21);
            this.cbTodosAlunos.TabIndex = 30;
            this.cbTodosAlunos.SelectedIndexChanged += new System.EventHandler(this.cbTodosAlunos_SelectedIndexChanged);
            // 
            // lblTodosAlunos
            // 
            this.lblTodosAlunos.AutoSize = true;
            this.lblTodosAlunos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTodosAlunos.Location = new System.Drawing.Point(18, 7);
            this.lblTodosAlunos.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTodosAlunos.Name = "lblTodosAlunos";
            this.lblTodosAlunos.Size = new System.Drawing.Size(100, 13);
            this.lblTodosAlunos.TabIndex = 29;
            this.lblTodosAlunos.Text = "Todos os alunos";
            // 
            // btnFechar2
            // 
            this.btnFechar2.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar2.Image")));
            this.btnFechar2.Location = new System.Drawing.Point(558, 343);
            this.btnFechar2.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar2.Name = "btnFechar2";
            this.btnFechar2.Size = new System.Drawing.Size(36, 33);
            this.btnFechar2.TabIndex = 37;
            this.btnFechar2.UseVisualStyleBackColor = true;
            this.btnFechar2.Click += new System.EventHandler(this.btnFechar2_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 16);
            this.label3.TabIndex = 38;
            this.label3.Text = "Aluno seleccionado";
            // 
            // Ver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 387);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnFechar2);
            this.Controls.Add(this.dgvAluno);
            this.Controls.Add(this.lboxFiltro);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbTodosAlunos);
            this.Controls.Add(this.lblTodosAlunos);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Ver";
            this.Text = "Ver";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAluno)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAluno;
        private System.Windows.Forms.ListBox lboxFiltro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTodosAlunos;
        private System.Windows.Forms.Label lblTodosAlunos;
        private System.Windows.Forms.Button btnFechar2;
        private System.Windows.Forms.Label label3;
    }
}