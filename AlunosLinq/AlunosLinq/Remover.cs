﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace AlunosLinq
{
    public partial class Remover : Form
    {
        List<Aluno> alunoscopiados;

        public Remover(List<Aluno> alunos)
        {
            InitializeComponent();
            alunoscopiados = alunos;
            InitCombo();
        }

        private void InitCombo()
        {
            cbRemover.DataSource = alunoscopiados;
            cbRemover.DisplayMember = alunoscopiados.ToString();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (cbRemover.SelectedIndex == -1)
            {
                MessageBox.Show("Não escolheu nenhum aluno");
                return;
            }
            alunoscopiados.RemoveAt(cbRemover.SelectedIndex);
            MessageBox.Show("Aluno removido dos registos");
            Close();
        }

        private void btnFechar4_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
