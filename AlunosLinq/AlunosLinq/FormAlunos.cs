﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

namespace AlunosLinq
{
    public partial class FormAlunos : Form
    {
        private List<Aluno> alunos ;
        private Criar fCriar;
        private Ver fVer;
        private Editar fEditar;
        private Remover fRemover;

        public FormAlunos()
        {
            InitializeComponent();
            alunos = new List<Aluno>();
            if (!ImportarAlunos())
                alunos = ListaDeAlunos.loadLista();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private bool GravarAlunos()
        { 
            string ficheiro = @"Alunos.txt";
            StreamWriter sw = new StreamWriter(ficheiro, false);
            try
            {
                if (!File.Exists(ficheiro))
                    sw = File.CreateText(ficheiro);
                foreach (var aluno in alunos)
                {
                    string linha = string.Format("{0};{1};{2};{3};{4}", aluno.IdAluno,
                    aluno.PrimeiroNome, aluno.Apelido, aluno.DataNascimento, aluno.DisciplinasFeitas);
                    sw.WriteLine(linha);
                }
                sw.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }

        private bool ImportarAlunos()
        {
            string ficheiro = @"Alunos.txt";
            StreamReader sr;
            try
            {
                if (File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);
                    string linha = "";
                    while ((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[5];
                        campos = linha.Split(';');
                        var aluno = new Aluno
                        {
                            IdAluno = Convert.ToInt32(campos[0]),
                            PrimeiroNome = campos[1],
                            Apelido = campos[2],
                            DataNascimento = Convert.ToDateTime(campos[3]),
                            DisciplinasFeitas = Convert.ToInt32(campos[4])
                        };
                        alunos.Add(aluno);
                    }
                    sr.Close();
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            fCriar = new Criar(alunos);
            fCriar.Show();

        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            fVer = new Ver(alunos);
            fVer.Show();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            fEditar = new Editar(alunos);
            fEditar.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            fRemover = new Remover(alunos);
            fRemover.Show();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (GravarAlunos())
            {
                MessageBox.Show("Alunos gravados com sucesso");
            }
            Close();
        }
    }
}
