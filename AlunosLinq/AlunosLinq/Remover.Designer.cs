﻿namespace AlunosLinq
{
    partial class Remover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Remover));
            this.btnFechar4 = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRemover = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnFechar4
            // 
            this.btnFechar4.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar4.Image")));
            this.btnFechar4.Location = new System.Drawing.Point(272, 194);
            this.btnFechar4.Margin = new System.Windows.Forms.Padding(2);
            this.btnFechar4.Name = "btnFechar4";
            this.btnFechar4.Size = new System.Drawing.Size(33, 36);
            this.btnFechar4.TabIndex = 25;
            this.btnFechar4.UseVisualStyleBackColor = true;
            this.btnFechar4.Click += new System.EventHandler(this.btnFechar4_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.Location = new System.Drawing.Point(223, 194);
            this.btnConfirmar.Margin = new System.Windows.Forms.Padding(2);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(33, 36);
            this.btnConfirmar.TabIndex = 24;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 19);
            this.label1.TabIndex = 23;
            this.label1.Text = "Seleccionar o aluno:";
            // 
            // cbRemover
            // 
            this.cbRemover.FormattingEnabled = true;
            this.cbRemover.Location = new System.Drawing.Point(32, 102);
            this.cbRemover.Margin = new System.Windows.Forms.Padding(2);
            this.cbRemover.Name = "cbRemover";
            this.cbRemover.Size = new System.Drawing.Size(244, 21);
            this.cbRemover.TabIndex = 22;
            // 
            // Remover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 241);
            this.Controls.Add(this.btnFechar4);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbRemover);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Remover";
            this.Text = "Remover";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFechar4;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbRemover;
    }
}