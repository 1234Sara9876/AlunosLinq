﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace AlunosLinq
{
    public partial class Criar : Form
    {
        private List<Aluno> alunoscopiados;

        public Criar(List<Aluno> alunos)
        {
            InitializeComponent();
            alunoscopiados = alunos;
            tbIdAluno.Text = GeraIdPack().ToString();
        }

        private int GeraIdPack()
        {
            if (alunoscopiados.Count > 0)
                return alunoscopiados[alunoscopiados.Count-1].IdAluno + 1;
            else
                return 1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbPrimeiroNome.Text))
            {
                MessageBox.Show("Tem que inserir o primeiro nome do aluno");
                return;
            }
            if (string.IsNullOrEmpty(tbApelido.Text))
            {
                MessageBox.Show("Tem que inserir o apelido do aluno");
                return;
            }
            if (string.IsNullOrEmpty(tbDataNasc.Text))
            {
                MessageBox.Show("Tem que inserir a data de nascimento do aluno");
                return;
            }
            alunoscopiados.Add( new Aluno{ IdAluno =Convert.ToInt16(tbIdAluno.Text), PrimeiroNome = tbPrimeiroNome.Text, Apelido= tbApelido.Text, DataNascimento= Convert.ToDateTime(tbDataNasc.Text), DisciplinasFeitas=0} );
            Close();
        }

        private void btnFechar1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
