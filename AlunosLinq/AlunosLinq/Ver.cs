﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using ClassLibrary1;
using System.Linq;

namespace AlunosLinq
{

    public partial class Ver : Form
    {
        List<Aluno> alunoscopiados;
        int contador=0;

        public Ver(List<Aluno> alunos)
        {
            InitializeComponent();
            alunoscopiados = alunos;
            InitCombo();
            Listar();
        }

        private void InitCombo()
        {
            cbTodosAlunos.DataSource = alunoscopiados;
            cbTodosAlunos.DisplayMember = alunoscopiados.ToString();
        }

        private void ApagarDados()
        {
            dgvAluno.Columns.Clear();
            dgvAluno.Rows.Clear();
            lboxFiltro.DataSource = null;
        }

        private void Listar()
        {
            lboxFiltro.DataSource = alunoscopiados.Where(x => x.DisciplinasFeitas
            >=1).OrderBy(x => x.PrimeiroNome).ThenBy(x => x.Apelido).ToList();
            //to list pq se transforma uma lista feita com linq numa List<>
            lboxFiltro.DisplayMember = "NomeCompleto";
        }

        private void btnFechar2_Click(object sender, System.EventArgs e)
        {
            ApagarDados();
            Close();
        }

        private void cbTodosAlunos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (contador == 0)
            {
                contador++;
                return;
            }
            else
            {
                Aluno student = (Aluno)cbTodosAlunos.SelectedItem;
                selectedData(student);
                contador++;
            }
        }

        private void selectedData(Aluno alunoSelecionado)
        {
            dgvAluno.Columns.Add("colId", "IdAluno");
            dgvAluno.Columns.Add("colNome", "PrimeiroNome");
            dgvAluno.Columns.Add("colApelido", "Apelido");
            dgvAluno.Columns.Add("colData", "DataNascimento");
            dgvAluno.Columns.Add("colFeitas", "DisciplinasFeitas");

            dgvAluno.Rows.Add(alunoSelecionado.IdAluno, alunoSelecionado.PrimeiroNome, alunoSelecionado.Apelido, alunoSelecionado.DataNascimento, alunoSelecionado.DisciplinasFeitas);

            dgvAluno.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
    }
}
