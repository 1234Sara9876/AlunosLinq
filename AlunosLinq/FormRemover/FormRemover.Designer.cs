﻿namespace FormRemover
{
    partial class FormRemover
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRemover));
            this.btnFechar4 = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRemover = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnFechar4
            // 
            this.btnFechar4.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar4.Image")));
            this.btnFechar4.Location = new System.Drawing.Point(379, 259);
            this.btnFechar4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFechar4.Name = "btnFechar4";
            this.btnFechar4.Size = new System.Drawing.Size(44, 44);
            this.btnFechar4.TabIndex = 21;
            this.btnFechar4.UseVisualStyleBackColor = true;
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.Location = new System.Drawing.Point(314, 259);
            this.btnConfirmar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(44, 44);
            this.btnConfirmar.TabIndex = 19;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(108, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 23);
            this.label1.TabIndex = 18;
            this.label1.Text = "Seleccionar o aluno:";
            // 
            // cbRemover
            // 
            this.cbRemover.FormattingEnabled = true;
            this.cbRemover.Location = new System.Drawing.Point(60, 145);
            this.cbRemover.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbRemover.Name = "cbRemover";
            this.cbRemover.Size = new System.Drawing.Size(324, 24);
            this.cbRemover.TabIndex = 17;
            // 
            // FormRemover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 303);
            this.ControlBox = false;
            this.Controls.Add(this.btnFechar4);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbRemover);
            this.Name = "FormRemover";
            this.Text = "Apagar aluno";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnFechar4;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbRemover;
    }
}

