﻿namespace FormEditar
{
    partial class FormEditar
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditar));
            this.udPreco2 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.rtbDesc2 = new System.Windows.Forms.RichTextBox();
            this.tbIDPack2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.udPreco2)).BeginInit();
            this.SuspendLayout();
            // 
            // udPreco2
            // 
            this.udPreco2.DecimalPlaces = 2;
            this.udPreco2.Location = new System.Drawing.Point(234, 223);
            this.udPreco2.Margin = new System.Windows.Forms.Padding(4);
            this.udPreco2.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            131072});
            this.udPreco2.Name = "udPreco2";
            this.udPreco2.Size = new System.Drawing.Size(117, 22);
            this.udPreco2.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(360, 223);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 28);
            this.label2.TabIndex = 23;
            this.label2.Text = "€";
            // 
            // btnGravar
            // 
            this.btnGravar.Image = ((System.Drawing.Image)(resources.GetObject("btnGravar.Image")));
            this.btnGravar.Location = new System.Drawing.Point(393, 329);
            this.btnGravar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(44, 44);
            this.btnGravar.TabIndex = 21;
            this.btnGravar.UseVisualStyleBackColor = true;
            // 
            // btnForward
            // 
            this.btnForward.Image = ((System.Drawing.Image)(resources.GetObject("btnForward.Image")));
            this.btnForward.Location = new System.Drawing.Point(210, 317);
            this.btnForward.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(35, 44);
            this.btnForward.TabIndex = 20;
            this.btnForward.UseVisualStyleBackColor = true;
            // 
            // btnBack
            // 
            this.btnBack.Image = ((System.Drawing.Image)(resources.GetObject("btnBack.Image")));
            this.btnBack.Location = new System.Drawing.Point(92, 317);
            this.btnBack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(31, 44);
            this.btnBack.TabIndex = 19;
            this.btnBack.UseVisualStyleBackColor = true;
            // 
            // rtbDesc2
            // 
            this.rtbDesc2.Location = new System.Drawing.Point(40, 83);
            this.rtbDesc2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rtbDesc2.Name = "rtbDesc2";
            this.rtbDesc2.Size = new System.Drawing.Size(347, 106);
            this.rtbDesc2.TabIndex = 18;
            this.rtbDesc2.Text = "";
            // 
            // tbIDPack2
            // 
            this.tbIDPack2.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDPack2.Location = new System.Drawing.Point(234, 38);
            this.tbIDPack2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbIDPack2.Name = "tbIDPack2";
            this.tbIDPack2.ReadOnly = true;
            this.tbIDPack2.Size = new System.Drawing.Size(52, 22);
            this.tbIDPack2.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "ID Pack";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 373);
            this.ControlBox = false;
            this.Controls.Add(this.udPreco2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.btnForward);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.rtbDesc2);
            this.Controls.Add(this.tbIDPack2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Editar";
            ((System.ComponentModel.ISupportInitialize)(this.udPreco2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown udPreco2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.RichTextBox rtbDesc2;
        private System.Windows.Forms.TextBox tbIDPack2;
        private System.Windows.Forms.Label label1;
    }
}

