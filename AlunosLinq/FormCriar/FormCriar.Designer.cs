﻿namespace FormCriar
{
    partial class FormCriar
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCriar));
            this.udPreco = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnFechar1 = new System.Windows.Forms.Button();
            this.rtbDesc = new System.Windows.Forms.RichTextBox();
            this.tbIDPack = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.udPreco)).BeginInit();
            this.SuspendLayout();
            // 
            // udPreco
            // 
            this.udPreco.DecimalPlaces = 2;
            this.udPreco.Location = new System.Drawing.Point(293, 243);
            this.udPreco.Margin = new System.Windows.Forms.Padding(4);
            this.udPreco.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            131072});
            this.udPreco.Name = "udPreco";
            this.udPreco.Size = new System.Drawing.Size(113, 22);
            this.udPreco.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(415, 245);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 28);
            this.label2.TabIndex = 15;
            this.label2.Text = "€";
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(348, 328);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(53, 41);
            this.btnSave.TabIndex = 14;
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnFechar1
            // 
            this.btnFechar1.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar1.Image")));
            this.btnFechar1.Location = new System.Drawing.Point(420, 328);
            this.btnFechar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFechar1.Name = "btnFechar1";
            this.btnFechar1.Size = new System.Drawing.Size(48, 41);
            this.btnFechar1.TabIndex = 13;
            this.btnFechar1.UseVisualStyleBackColor = true;
            // 
            // rtbDesc
            // 
            this.rtbDesc.Location = new System.Drawing.Point(29, 66);
            this.rtbDesc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rtbDesc.Name = "rtbDesc";
            this.rtbDesc.Size = new System.Drawing.Size(401, 142);
            this.rtbDesc.TabIndex = 12;
            this.rtbDesc.Text = "";
            // 
            // tbIDPack
            // 
            this.tbIDPack.BackColor = System.Drawing.SystemColors.Window;
            this.tbIDPack.Location = new System.Drawing.Point(263, 21);
            this.tbIDPack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbIDPack.Name = "tbIDPack";
            this.tbIDPack.ReadOnly = true;
            this.tbIDPack.Size = new System.Drawing.Size(52, 22);
            this.tbIDPack.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(147, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "ID Pack";
            // 
            // FormCriar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 390);
            this.ControlBox = false;
            this.Controls.Add(this.udPreco);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnFechar1);
            this.Controls.Add(this.rtbDesc);
            this.Controls.Add(this.tbIDPack);
            this.Controls.Add(this.label1);
            this.Name = "FormCriar";
            this.Text = "Criar";
            ((System.ComponentModel.ISupportInitialize)(this.udPreco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown udPreco;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnFechar1;
        private System.Windows.Forms.RichTextBox rtbDesc;
        private System.Windows.Forms.TextBox tbIDPack;
        private System.Windows.Forms.Label label1;
    }
}

