﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class ListaDeAlunos
    {    
        //a lista estática pode ser gravada, mas fora da classe
        public static  List<Aluno> loadLista()
        {
            List<Aluno>lista = new List<Aluno>();

            lista.Add(new Aluno { IdAluno = 1, PrimeiroNome = "Torres", Apelido = "Carlos", DataNascimento = Convert.ToDateTime("25/2/1970"), DisciplinasFeitas = 20 });
            lista.Add(new Aluno { IdAluno = 2, PrimeiroNome = "Jesuita", Apelido = "Susana", DataNascimento = Convert.ToDateTime("31/3/1970"), DisciplinasFeitas = 12 });
            lista.Add(new Aluno { IdAluno = 3, PrimeiroNome = "Susana", Apelido = "Sousa", DataNascimento = Convert.ToDateTime("3/1/1970"), DisciplinasFeitas = 1 });
            lista.Add(new Aluno { IdAluno = 4, PrimeiroNome = "Sara", Apelido = "Jesuita", DataNascimento = Convert.ToDateTime("6/3/1970"), DisciplinasFeitas = 8 });
            lista.Add(new Aluno { IdAluno = 5, PrimeiroNome = "Janota", Apelido = "Duarte", DataNascimento = Convert.ToDateTime("18/2/1970"), DisciplinasFeitas = 0 });
            lista.Add(new Aluno { IdAluno = 6, PrimeiroNome = "Maria", Apelido = "Susana", DataNascimento = Convert.ToDateTime("23/1/1970"), DisciplinasFeitas = 16 });

            return lista;
        }
    }
}
