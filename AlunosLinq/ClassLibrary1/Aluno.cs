﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Aluno
    {
        #region Atributos e Propriedades condensados


        public int IdAluno { get; set; }

        public string PrimeiroNome { get; set; }

        public string Apelido { get; set; }

        public DateTime DataNascimento { get; set; }

        public int DisciplinasFeitas { get; set; }

        #endregion

        #region Métodos Genéricos

        //public string NomeCompleto
        //{
        //  return string.Format("{0} {1}", PrimeiroNome, Apelido); 
        //}   ...pode ser substituído por uma expressão lambda...

        //nesta propriedade-método, => significa "return"
        public string NomeCompleto => $"{PrimeiroNome}{Apelido}";

        //public override string ToString()
        //{
        //    return string.Format("Id Aluno: {0}  Nome: {1}", IdAluno, NomeCompleto);
        //}   ...em vez do return string.Format...

        public override string ToString() => $"{IdAluno}{NomeCompleto}";
        #endregion

    }
}
